package com.codeoftheweb.salvo;

import com.codeoftheweb.salvo.models.Game;
import com.codeoftheweb.salvo.models.GamePlayer;
import com.codeoftheweb.salvo.repositories.GamePlayerRepository;
import com.codeoftheweb.salvo.repositories.GameRepository;
import com.codeoftheweb.salvo.repositories.PlayerRepository;
import com.codeoftheweb.salvo.repositories.ShipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/api")
public class AppController {
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private GamePlayerRepository gamePlayerRepository;
    @Autowired
    private ShipRepository shipRepository;

    @RequestMapping("/games")
    public List<Object> getAllGames(){
        return gameRepository.findAll().stream().map(game -> game.toDTO()).collect(Collectors.toList());
    }

    @RequestMapping("/players")
    public List<Object> getAllPlayers(){
        return playerRepository.findAll().stream().map(player -> player.toDTO()).collect(Collectors.toList());
    }

    @RequestMapping("/game_view/{gPlayerId}")
    public Map<String, Object> gameView(@PathVariable Long gPlayerId){
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        GamePlayer gamePlayer = gamePlayerRepository.getOne(gPlayerId);
        dto = gamePlayer.getGame().toDTO();
        dto.put("ships", gamePlayer.getShips().stream().map(ship -> ship.toDTO()).collect(Collectors.toList()));
        dto.put("salvoes", gamePlayer.getGame().getGamePlayers().stream().flatMap(gamePlayer1 -> gamePlayer1.getSalvoes().stream().map(salvo -> salvo.toDTO())).collect(Collectors.toList()));
        return dto;
    }

}
