package com.codeoftheweb.salvo;

import com.codeoftheweb.salvo.models.*;
import com.codeoftheweb.salvo.repositories.*;
import org.apache.tomcat.jni.Time;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.*;

@SpringBootApplication
public class SalvoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalvoApplication.class, args);
		
	}

	@Bean
	public CommandLineRunner initData(PlayerRepository playerRepository, GameRepository gameRepository, GamePlayerRepository gamePlayerRepository, ShipRepository shipRepository, SalvoRepository salvoRepository) {
		return (args) -> {
			for(int i=0; i<5; i++)
				playerRepository.save(new Player("mail" + i+1 + "@gmail.com"));

			List<Game> games = new ArrayList<>();
			for(int i=0; i<3; i++)
				games.add(new Game());

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(games.get(0).getCreationDate());

			calendar.add(Calendar.HOUR, +1);
			games.get(1).setCreationDate(calendar.getTime());

			calendar.add(Calendar.HOUR, +1);
			games.get(2).setCreationDate(calendar.getTime());

			gameRepository.saveAll(games);

			GamePlayer gamePlayer1 = new GamePlayer(playerRepository.getOne((long)1), gameRepository.getOne((long)1));
			GamePlayer gamePlayer2 = new GamePlayer(playerRepository.getOne((long)2), gameRepository.getOne((long)1));
			gamePlayerRepository.save(gamePlayer1);
			gamePlayerRepository.save(gamePlayer2);

			List<Ship> ships = new ArrayList<>();
			ships.add(new Ship("Cruiser", Arrays.asList("A1", "A2", "A3"), gamePlayer1));
			ships.add(new Ship("Destroyer", Arrays.asList("B1", "B2"), gamePlayer1));
			ships.add(new Ship("Battleship", Arrays.asList("C1", "C2", "C3", "C4"), gamePlayer1));
			ships.add(new Ship("Cruiser", Arrays.asList("F1", "F2", "F3"), gamePlayer1));
			ships.add(new Ship("Cruiser", Arrays.asList("A1", "B1", "C1"), gamePlayer2));
			ships.add(new Ship("Destroyer", Arrays.asList("F4", "F5"), gamePlayer2));

			shipRepository.saveAll(ships);
			
			List<Salvo> salvoes = new ArrayList<>();
			salvoes.add(new Salvo(1, Arrays.asList("B3", "D7"), gamePlayer1));
			salvoes.add(new Salvo(2, Arrays.asList("D2", "F1"), gamePlayer1));
			salvoes.add(new Salvo(1, Arrays.asList("F3", "A7"), gamePlayer2));
			salvoes.add(new Salvo(2, Arrays.asList("D3", "F6"), gamePlayer2));

			salvoRepository.saveAll(salvoes);

			gamePlayerRepository.save(new GamePlayer(playerRepository.getOne((long)3), gameRepository.getOne((long)2)));
		};
	}
}
